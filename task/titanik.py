import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    def extract_title(name):
        if 'Mr.' in name:
            return 'Mr.'
        elif 'Mrs.' in name:
            return 'Mrs.'
        elif 'Miss.' in name:
            return 'Miss.'
        else:
            return None

    df['Title'] = df['Name'].apply(extract_title)

    filled_ages = []

    for title in ["Mr.", "Mrs.", "Miss."]:
        title_mask = df['Title'] == title
        median_age = df.loc[title_mask, 'Age'].median()
        missing_ages_count = df.loc[title_mask, 'Age'].isnull().sum()

        if missing_ages_count > 0:
            df.loc[title_mask & df['Age'].isnull(), 'Age'] = median_age
            filled_ages.append((title, missing_ages_count, int(round(median_age))))
        else:
            filled_ages.append((title, missing_ages_count, int(median_age)))

    return filled_ages
